<?php

//print_r($_FILES['archivo']);
//echo "<br>";

$archivo = $_FILES['archivo'];
$nombre = $archivo['name'];
$tipo = $archivo['type'];
$tamano = $archivo['size'];

if($tamano<=10485760){
    if($tipo == "image/jpeg" || $tipo == "image/png" || $tipo == "image/jpg" || $tipo == "image/gif"){
        if(!is_dir('imagenes')):
        mkdir('imagenes',0777);
        endif;
        move_uploaded_file($archivo['tmp_name'],"imagenes/".$nombre);
        header("Refresh: 5; URL-index.php");
        echo "<h1>Imagen subida correctamente</h1>";
    }else{
    header("Refresh: 5; URL-index.php");
    echo "<h1>El archivo no es el correcto</h1>";
}
}else{
    header("Refresh: 5; URL-index.php");
    echo "<h1>El archivo es muy grande para subirlo</h1>";
}