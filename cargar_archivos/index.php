<!DOCTYPE>
<html>
    <head>
        <title>Cargar Archivos</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width-device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Formulario para subir imagenes</h1>
        <div>
            <form action="subir.php" method="POST" enctype="multipart/form-data">
                <input type="file" name="archivo">
                <br><br>
                <input type="submit" value="Enviar">
            </form>
            <h1>Listado de imagenes</h1>
            <?php
            $gestor = opendir("./imagenes");//abrimos archivos
            if($gestor): //preguntamos si existe algo en el archivo
                while(($images = readdir($gestor))!== false)://realizamos un bucle mientras existan archivos
                    if($images != "." && $images != ".." )://no salga de archivo existente
                        echo "<img src='imagenes/$images' width='200px'>";
                    endif;
                endwhile;
            endif;
            ?>
        </div>
    </body>
</html>
        