<?php

//El arreglo es un conjunto de datos utilizados en una sola variable, contenedor de datos

// $arreglo = array(elementos);
// $zapatos = [elementos];

$zapatillas = array("nike","rebook","adidas","jordan");
var_dump($zapatillas);

echo "<br>";

print_r($zapatillas);

echo "<br>";

echo $zapatillas[0]."<br>";

//foreach ($zapatillas as $key => $value) {
//    
//}
// se utiliza con un identificador
echo "<ul>";
foreach ($zapatillas as $zapatilla ) {
    echo "<li>$zapatilla"."<br>";
}
echo "/ul";