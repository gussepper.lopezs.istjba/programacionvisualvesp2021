<?php
//Sesioses.- Son datos que perduran a traves de la navegacion en el sitio web

//Para funcionar una sesion en un sitio web es necesario:

//1.-

session_start();

?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Formulario en PHP</title>
    </head>
    <body>
        <form action="recibir.php" method="GET">
            <p>
                <label for="nombre">Nombre</label>
                <input type="text" id="nombre" name="nombre" placeholder="Ingrese el nombre:">
            </p>
            <p>
                <label for="apellido">Apellido</label>
                <input type="text" id="apellido" name="apellido" placeholder="Ingrese el apellido:">
            </p>
            <p>
                <input type="submit" value="Enviar datos">
            </p>
            <?php
            echo $variable."<br>";
            echo $_SESSION['variableSession']."<br>";
            ?>
            <a href="logout.php">Cerrar</a>
        </form>    
    </body>
</html>
