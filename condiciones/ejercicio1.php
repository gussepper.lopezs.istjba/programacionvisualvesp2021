<?php

echo "<h1>Ejercicio con IF</h1>";
$par = $_GET['par'];
if($par%2 == 0):
    echo "El numero es par";
    else:
    echo "El numero es impar";
endif;

echo "<h1>Ejercicio con Else-If</h1>";
$edad = $_GET ['edad'];   
if ($edad <= 17) {
    echo "Menor de edad";
} elseif ($edad <= 59){
    echo "Mayor de edad";
} elseif ($edad <= 100) {
    echo "Tercera edad";
}else{
    echo "excede el rango de edad";
}


echo "<h1>Ejercicio con Switch-case</h1>";
echo "<br>";
echo "Cual es la capital de Ecuador?";
echo "<br>";
echo "<br>1. Guayaquil<br>";
echo "2. Cuenca<br>";
echo "3. Quito<br>";
echo "4. Manabi<br>";
echo "<br>";
$respuesta = $_GET['respuesta'];

switch($respuesta){
    case 1:
        echo "Respuesta Incorrecta";
    break;
    case 2:
        echo "Respuesta Incorrecta";
    break;
    case 3:
        echo "Respuesta Correcta";
    break;
    case 4:
        echo "Respuesta Incorrecta";
    break;
    default:
        echo "Respuesta no valida";
    break;
}


