<?php
include './includes/cabecera.php';
include './includes/lateral.php';
?>
            
            <div id="principal">
                <h3>Crear Entradas</h3>
                <p>Esta pantalla nos permite crear una entrada</p>
                <br>
                <hr><!-- Creamos el formulario -->
                <?php if(isset($_SESSION['completo_entrada'])):?>
                <div class="alerta alerta-exito">
                    <?= $_SESSION['completo_entrada']?>
                </div>
                <?php elseif(isset($_SESSION['error_entrada'])):?>
                <div class="alerta alerta-error">
                    <?= $_SESSION['error_categoria']?>
                </div>
                <?php endif;?>
                <form action="guardar-entradas.php" method="POST">
                    <label>Ingrese el titulo de la entrada</label>
                    <input type="text" name="titulo">
                    <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'titulo'): '';?>
                    <label>Ingrese la descripcion que llevara la entrada</label>
                    <textarea id="descripcion" name="descripcion"></textarea>
                    <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'descripcion'): '';?>
                    <label>Seleccione la categoria</label>
                    <select name="categoria" id="categoria">
                        <?php 
                            $categorias = conseguirCategorias($db);
                            //print_r($categorias);die();
                            if (!empty($categorias)):
                                while($categoria = mysqli_fetch_assoc($categorias)):
                        ?>        
                                    <option value="<?= $categoria['id']; ?>"><?= $categoria['nombre']; ?></option>
                        <?php
                                endwhile;
                            endif;    
                        ?>
                    </select>
                    <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'categoria'): '';?>
                    <input type="submit" value="Guardar">
                </form>
                <?php borrarErrores();?>
            </div>
<?php
include './includes/pie.php';
?>