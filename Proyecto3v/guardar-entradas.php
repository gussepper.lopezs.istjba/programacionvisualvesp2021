<?php

session_start();

if(isset($_POST)){
    //incluir la conexion a la base de datos
    include './includes/conexion.php';
    //recoger los datos del formulario
    $titulo = isset($_POST['titulo']) ? mysqli_real_escape_string($db,$_POST['titulo']): false;
    $descripcion = isset($_POST['descripcion']) ? mysqli_real_escape_string($db,$_POST['descripcion']): false;
    $categoria = isset($_POST['categoria']) ? mysqli_real_escape_string($db,$_POST['categoria']): false;
    $usuario = $_SESSION['usuario']['id'];
    $errores = array();
    //validar campos
    if(!empty($titulo) && !is_numeric($titulo)){
        $titulo_valido = true;
    }else{
        $titulo_valido = false;
        $errores['titulo'] = "El titulo no es valido";
    }
    if(!empty($descripcion) && !is_numeric($descripcion)){
        $descripcion_valido = true;
    }else{
        $descripcion_valido = false;
        $errores['descripcion'] = "La descripcion no es valido";
    }if($categoria != 0){
        $descripcion_valido = true;
    }else{
        $descripcion_valido = false;
        $errores['descripcion'] = "La descripcion no es valido";
    }
    //insertar en la base de datos
    if(count($errores) == 0){
        $sql = "insert into entradas values(null,$usuario,$categoria,'$titulo','$descripcion',curdate())";
        $guardar = mysqli_query($db,$sql);
        if($guardar){
            $_SESSION['completo_entrada'] = 'La entrada se guardo con exito';
        }else{
            $_SESSION['error_entrada'] = 'Error al momento de guardar la entrada';
        }
    }else{
        $_SESSION['errores'] = $errores;
    }
}

header("Location: crear-entradas.php");