<aside id="sidebar">
                <?php if(isset($_SESSION['usuario'])):?>
                <div id="buscador" class="bloque">
                    <h3>Buscar</h3>
                    <form action="" method="">
                        <input type="text" name="busqueda">
                        <input type="submit" value="buscar">
                    </form>
                </div>
                <div id="usuario_logeado" class="bloque">
                    <h3>Bienvenido <?=$_SESSION['usuario']['nombre']." ".$_SESSION['usuario']['apellidos']?></h3>
                    <a href="crear-catalogo.php" class="boton">Crear Catalogo</a>
                    <a href="crear-entradas.php" class="boton boton-naranja">Crear Entrada</a>
                    <a href="#" class="boton boton-verde">Mi perfil</a>
                    <a href="cerrar.php" class="boton boton-rojo">Cerrar</a>
                </div>
                <?php endif;?>
                <?php if(!isset($_SESSION['usuario'])):?>
                <div id="login" class="bloque">
                    <h3>Login</h3>
                    <?php if(isset($_SESSION['error_login'])):?>
                    <div class="alerta alerta-error">
                        <?=$_SESSION['error_login']?>
                    </div>
                    <?php endif;?>
                    <form action="login.php" method="POST">
                        <label for="email">Email</label>
                        <input type="email" id="email" name="email">
                        <label for="password">Contraseña</label>
                        <input type="password" id="password" name="password">
                        <input type="submit" value="Entrar">
                    </form>
                </div>
                <div id="register" class="bloque">
                    <h3>Registro</h3>
                    <?php if(isset($_SESSION['completado'])):?>
                    <div class="alerta alerta-exito">
                        <?= ($_SESSION['completado']) ?>
                    </div>
                    <?php elseif(isset($_SESSION['errores']['general'])): ?>
                    <div class="alerta alerta-error">
                        <?= ($_SESSION['errores']['general']) ?>
                    </div>
                    <?php endif; ?>
                    <form action="registro.php" method="POST">
                        <label for="name">Nombre</label>
                        <input type="text" id="name" name="name">
                        <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'nombre'): ''; ?>
                        <label for="apellidos">Apellidos</label>
                        <input type="text" id="apellidos" name="apellidos">
                        <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'apellidos'): ''; ?>
                        <label for="email">Email</label>
                        <input type="text" id="email" name="email">
                        <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'email'): ''; ?>
                        <label for="password">Contraseña</label>
                        <input type="password" id="password" name="password">
                        <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'password'): ''; ?>
                        <input type="submit" value="Registrar">
                    </form>
                    <?php borrarErrores(); ?>
                </div>
                <?php endif; ?>
            </aside>

