<?php
function mostrarError($errores,$campo){
    $alerta='';
    if(isset($errores[$campo]) && !empty($errores[$campo])){
        $alerta = "<div class='alerta alerta-error'>.$errores[$campo].</div>";
    }
    return $alerta;
}

function borrarErrores(){
    $borrado = false;
    if(isset($_SESSION['errores']['general'])){
        $_SESSION['errores']['general'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['completado'])){
        $_SESSION['completado'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['errores'])){
        $_SESSION['errores'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['error_login'])){
        $_SESSION['error_login'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['error_categoria'])){
        $_SESSION['error_categoria'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['completo_categoria'])){
        $_SESSION['completo_categoria'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['error_entrada'])){
        $_SESSION['error_entrada'] = null;
        $borrado = true;
    }
    if(isset($_SESSION['completo_entrada'])){
        $_SESSION['completo_entrada'] = null;
        $borrado = true;
    }
}
function conseguirCategorias($conexion){
    $sql = "select * from categorias order by id";
    $categorias = mysqli_query($conexion,$sql);
    $resultado = array();
    if($categorias && mysqli_num_rows($categorias)){
        $resultado=$categorias;
    }
    return $resultado;
}