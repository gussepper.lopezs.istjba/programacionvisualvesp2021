<?php
    include './includes/conexion.php';
    include './includes/funciones.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Proyecto 3ro Vespertina</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="asset/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
<!--        CABECERA -->
        <header id="cabecera">
<!--        LOGO -->
            <div id="logo">
                <a href="index.php">
                    Blog VideJuegos
                </a>
            </div>
<!--        MENU -->
            <nav id="menu">
                <ul>
                    <li><a href="index.php">Inicio</a></li>
                    <?php
                        $categorias = conseguirCategorias($db);
                        if(!empty($categorias)):
                            while($categoria = mysqli_fetch_assoc($categorias)):
                    ?>
                    <li><a href="categoria.php?id=<?=$categoria['id']?>"><?=$categoria['nombre']?></a></li>
                    <?php
                    endwhile;
                    endif;
                    ?>
                    <li><a href="index.php">Salir</a></li>
                </ul>
            </nav>
            <div class="clearfix"></div>
        </header>
        <div id="contenedor">