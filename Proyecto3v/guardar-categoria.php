<?php

session_start();

if(isset($_POST)){
    //incluir la conexion a la base de datos
    include './includes/conexion.php';
    //recoger los datos del formulario
    $nombre = isset($_POST['nombre']) ? mysqli_real_escape_string($db,$_POST['nombre']): false;
    $errores = array();
    //validar campos
    if(!empty($nombre) && !is_numeric($nombre)){
        $nombre_valido = true;
    }else{
        $nombre_valido = false;
        $errores['nombre'] = "El nombre no es valido";
    }
    //insertar en la base de datos
    if(count($errores) == 0){
        $sql = "insert into categorias values(null,'$nombre')";
        $guardar = mysqli_query($db,$sql);
        if($guardar){
            $_SESSION['completo_categoria'] = 'La categoria se guardo con exito';
        }else{
            $_SESSION['error_categoria'] = 'Error al momento de guardar categoria';
        }
    }else{
        $_SESSION['errores'] = $errores;
    }
}

header("Location: crear-catalogo.php");