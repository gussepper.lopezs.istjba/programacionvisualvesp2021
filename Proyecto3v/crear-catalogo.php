<?php
include './includes/cabecera.php';
include './includes/lateral.php';
?>
            
            <div id="principal">
                <h3>Crear Catalogo</h3>
                <p>Esta pantalla nos permite crear un Catalogo</p>
                <br>
                <hr><!-- Creamos el formulario -->
                 <?php if(isset($_SESSION['completo_categoria'])):?>
                <div class="alerta alerta-exito">
                    <?= $_SESSION['completo_categoria']?>
                </div>
                <?php elseif(isset($_SESSION['error_categoria'])):?>
                <div class="alerta alerta-error">
                    <?= $_SESSION['error_categoria']?>
                </div>
                <?php endif;?>
                <form action="guardar-categoria.php" method="POST">
                    <label>Ingrese el nombre de la categoria</label>
                    <input type="text" name="nombre">
                    <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'nombre'): '';?>
                    <input type="submit" value="Guardar">
                </form>
                <?php borrarErrores();?>
            </div>
        
<?php
include './includes/pie.php';
?>