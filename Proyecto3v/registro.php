<?php

$errores = array();
if(isset($_POST)){
    //Incluimos la conexion
    include './includes/conexion.php';
    //Rescatamos los elementos del formulario
    if(!isset($_SESSION)){
        session_start();
    }
    
    $nombre = isset($_POST['name']) ? mysqli_real_escape_string($db,$_POST['name']): false;
    $apellidos = isset($_POST['apellidos']) ? mysqli_real_escape_string($db,$_POST['apellidos']): false;
    $email = isset($_POST['email']) ? mysqli_real_escape_string($db,$_POST['email']): false;
    $password = isset($_POST['password']) ? mysqli_real_escape_string($db,$_POST['password']): false;
    
    
    //validar campos
    if(!empty($nombre) && !is_numeric($nombre) && !preg_match("/[0-9]/", $nombre)){
        $nombre_valido = true;
    }else{
        $nombre_valido = false;
        $errores['nombre'] = "El nombre no es valido";
    }
    
    if(!empty($apellidos) && !is_numeric($apellidos) && !preg_match("/[0-9]/", $apellidos)){
        $apellidos_valido = true;
    }else{
        $apellidos_valido = false;
        $errores['apellidos'] = "El apellido no es valido";
    }
    
    if(!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)){
        $email_valido = true;
    }else{
        $email_valido = false;
        $errores['email'] = "El email no es valido";
    }
    
    if(!empty($password)){
        $password_valido = false;
    }else{
        $password_valido = false;
        $errores['password'] = "La contraseña esta vacia";
    }
    
    $guardar_usuario = false;
    
    if(count($errores)==0){
        //crear una sentencia sql que me arroje la linea del usuario
        $sql1 = "select * from usuarios where email = '$email'";
        // $consulta = ejecutamos el select
        $consulta = mysqli_query($db,$sql1);
        //condicion mysqli_num_row($consulta) == 0
        if(mysqli_num_rows($consulta) == 0){
            $guardar_usuario = true;
            //Cifrar contraseña
            //md5($strin5); sha($string)
            $password_segura = password_hash($password,PASSWORD_BCRYPT,['cost'=>4]);
            //echo $password_segura;die();
        
            //Insertar en la base de datos
            $sql = "insert into usuarios values(null,'$nombre','$apellidos','$email','$password_segura',CURDATE())";
            $guardar = mysqli_query($db,$sql);
        if($guardar){
            $_SESSION['completado'] = "El registro se guardo con exito";
        }else{
            $_SESSION['errores']['general'] = "Existe un error en el registro";
        }    
        }else{
            $_SESSION['errores']['general'] = "Ya existe este correo";
        }
    }else{
        $_SESSION['errores'] = $errores;
    }
}
header("Location: index.php");