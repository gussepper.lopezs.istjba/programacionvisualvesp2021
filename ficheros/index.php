<?php

// es un documento o medio de PHP que puede ser manipulado

//Operador	Descripción
//w	crea un fichero de escritura. Si ya existe lo crea de nuevo
//w+	crea un fichero de lectura y escritura. Si ya existe lo crea de nuevo
//a	abre o crea un fichero para añadir datos al final del mismo
//a+	abre o crea un fichero para leer y añadir datos al final del mismo
//r	abre un fichero de lectura
//r+	abre un fichero de lectura y escritura

$filename= "archivo_file.txt";
$mode= "a+";
$archivo= fopen($filename, $mode);


//final de archivo
while(!feof($archivo)){
    $contenido = fgets($archivo);
    echo "$contenido<br>";
}

$srtin = "<br>Estudiantes de 3 de programacion visual";
fwrite($archivo, $srtin);

//cerrar archivo
//fclose($archivo);

//copiar archivo
//copy("archivo_file.txt","archivo_copiado.txt");

//renombrar un archivo
//rename("archivo_copiado.txt","archivo_renombrado.txt");

//eliminar archivo
//unlink("archivo_renombrado.txt");
